import axios from "axios";
import { inject, injectable } from "inversify";

import { IHttpClient } from "./iHttpClient";
import { ILogger } from "./iLogger";
import { COMMON_TYPES } from "../ioc/commonTypes";

@injectable()
export class HttpClient implements IHttpClient {

    @inject(COMMON_TYPES.ILogger)
    private readonly _logger: ILogger;

    public async get(url: string): Promise<any> {
        try {
            const response: any = await axios.get(url);
            this._logger.info("HTTP - Data Fetched");
            return response.data;
        } catch (error) {
            this._logger.error(`${JSON.stringify(error)}`);
            throw {
                status: error.response.status,
                message: error.message,
            };
        }
    }
}
