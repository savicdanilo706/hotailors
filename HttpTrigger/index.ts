import { AzureFunction, Context, HttpRequest } from "@azure/functions";
import { Container } from "inversify";

import { IResponse } from "./interfaces";
import { IFunctionService } from "./services/IFunctionService";
import { ILogger } from "../commonServices/iLogger";
import { Logger } from "../commonServices/logger";
import { COMMON_TYPES } from "../ioc/commonTypes";
import getContainer from "../ioc/inversify.config";

const httpTrigger: AzureFunction = async (ctx: Context, req: HttpRequest): Promise<any> => {
    const container: Container = getContainer();
    const logger: Logger = container.get<ILogger>(COMMON_TYPES.ILogger) as Logger;
    logger.init(ctx, "1");

    const functionService: IFunctionService<any> = container.get<
        IFunctionService<any>
    >(COMMON_TYPES.IFunctionService);

    try {
        const response: IResponse = await functionService.processMessageAsync(req);

        ctx.res = {
            body: response.body,
            status: response.status,
            headers: { "Content-Type": "application/json" },
        };

        return ctx.res;
    } catch (error) {
        ctx.res = {
            body: { message: error.message },
            status: error.status,
            headers: { "Content-Type": "application/json" },
        };

        return ctx.res;
    }
};

export default httpTrigger;
