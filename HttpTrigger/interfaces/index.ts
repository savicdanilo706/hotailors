export interface IUserRequest {
    id: string;
    type: string;
}

export interface IResponse {
    status: number;
    body: any;
}

export interface IError {
    status: number;
    message: string;
}
