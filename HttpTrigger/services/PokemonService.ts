import { inject, injectable } from "inversify";
import { filter, flow, map } from "lodash/fp";

import { IPokemonService } from "./IPokemonService";
import { IError, IUserRequest } from "../interfaces";
import { IHttpClient } from "../../commonServices/iHttpClient";
import { ILogger } from "../../commonServices/iLogger";
import { COMMON_TYPES } from "../../ioc/commonTypes";

@injectable()
export class PokemonService implements IPokemonService {

    @inject(COMMON_TYPES.ILogger)
    private readonly _logger: ILogger;

    @inject(COMMON_TYPES.IHttpClient)
    private readonly _httpClient: IHttpClient;

    private readonly _pokemonUrl: string = "https://pokeapi.co/api/v2/type/";

    public reqFormatter(req: any): IUserRequest {
        if (req.query.type && req.query.id) {
            return {
                id: req.query.id.trim(),
                type: req.query.type.trim().toLowerCase(),
            };
        } else {
            const error: IError = {
                status: 400,
                message: "ID and Pokemon Type are required",
            };
            this._logger.error(`${JSON.stringify(error)}`);
            throw error;
        }
    }

    public async findMatchingPokemons(userRequest: IUserRequest): Promise<any> {
        const fetchedPokemons: any = await this._httpClient.get(`${this._pokemonUrl}${userRequest.type}`);

        const ids: string[] = userRequest.id.split(",");

        return flow(
            map((element: any) => {
                const urlSplit: string[] = element.pokemon.url.split("/");
                return {
                    name: element.pokemon.name,
                    id: urlSplit[urlSplit.length - 2],
                };
            }),
            filter((element) => ids.indexOf(element.id) !== -1),
            map((element) => element.name),
        )(fetchedPokemons.pokemon);
    }
}
