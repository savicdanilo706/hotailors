import { inject, injectable } from "inversify";

import { IFunctionService } from "./IFunctionService";
import { IPokemonService } from "./IPokemonService";
import { IUserRequest } from "../interfaces";
import { ILogger } from "../../commonServices/iLogger";
import { COMMON_TYPES } from "../../ioc/commonTypes";

@injectable()
export class FunctionService implements IFunctionService<any> {

    @inject(COMMON_TYPES.ILogger)
    private readonly _logger: ILogger;

    @inject(COMMON_TYPES.IPokemonService)
    private readonly _pokemonService: IPokemonService;

    public async processMessageAsync(req: any): Promise<any> {
        try {
            const formattedReq: IUserRequest = this._pokemonService.reqFormatter(req);
            const pokemons: string[] = await this._pokemonService.findMatchingPokemons(formattedReq);

            return { status: 200, body: { pokemons } };
        } catch (error) {
            this._logger.error(`${JSON.stringify(error)}`);
            throw error;
        }
    }
}
