import { IUserRequest } from "../interfaces";

export interface IPokemonService {
    reqFormatter(req: any): IUserRequest;
    findMatchingPokemons(userRequest: IUserRequest): Promise<any>;
}
