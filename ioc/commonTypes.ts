export const COMMON_TYPES: any = {
    ILogger: Symbol.for("ILogger"),
    IFunctionService: Symbol.for("IFunctionService"),
    IHttpClient: Symbol.for("IHttpClient"),
    IPokemonService: Symbol.for("IPokemonService"),
};
