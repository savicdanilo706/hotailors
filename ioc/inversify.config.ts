import "reflect-metadata";
import { Container } from "inversify";

import { COMMON_TYPES } from "./commonTypes";
import { HttpClient } from "../commonServices/httpClient";
import { IHttpClient } from "../commonServices/iHttpClient";
import { ILogger } from "../commonServices/iLogger";
import { Logger } from "../commonServices/logger";
import { FunctionService } from "../HttpTrigger/services/FunctionService";
import { IFunctionService } from "../HttpTrigger/services/IFunctionService";
import { IPokemonService } from "../HttpTrigger/services/IPokemonService";
import { PokemonService } from "../HttpTrigger/services/PokemonService";

const getContainer: () => Container = (): Container => {
    const container: Container = new Container();

    container.bind<ILogger>(COMMON_TYPES.ILogger).to(Logger).inSingletonScope();

    container
        .bind<IFunctionService<any>>(COMMON_TYPES.IFunctionService)
        .to(FunctionService);

    container.bind<IHttpClient>(COMMON_TYPES.IHttpClient).to(HttpClient);
    container
        .bind<IPokemonService>(COMMON_TYPES.IPokemonService)
        .to(PokemonService);

    return container;
};

export default getContainer;
